variable "region" {
    type = "string"
    default = "us-east-1"
}

variable "amis" {
    type = "map"
    default = {
        "us-east-1" = "ami-035b3c7efe6d061d5"
        "us-east-2" = "ami-05c1fa8df71875112"
    }
  
}
variable "iam_fleet_role" {
    type = "string"
    default = "arn:aws:iam::207117134565:role/aws-service-role/ec2fleet.amazonaws.com/AWSServiceRoleForEC2Fleet"
}

variable "key_pair" {
    type = "map"
    default = {
        "us-east-1" = "gabe_key_pair"
    }
}

variable "subnets_us-east-1" {
    type = "map"
    default = {
        "us-east-1a" = "subnet-d0a6fcb7"
        "us-east-1b" = "subnet-367a5a18"
        "us-east-1c" = "subnet-83a055ce"
        "us-east-1d" = "subnet-ae7157f2"
        "us-east-1e" = "subnet-1b6c0525"
    }
}

variable "lustre_subnet_us-east-1b" {
    type = "list"
    default = ["subnet-367a5a18"]
}