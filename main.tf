provider "aws" {
    region = var.region
    version = 2.25
}

resource "aws_s3_bucket" "terraform_state_libertytech" {
    bucket = "terraform-state-libertytech"
    versioning {
        enabled = true
    }
    tags = {
        Name = "Terraform state files"
    }
}

terraform {
    backend "s3" {
        bucket = "terraform-state-libertytech"
        key = "terraform.tfstate"
        region = "us-east-1"
    }
}

resource "aws_fsx_lustre_file_system" "hpc_storage" {
    storage_capacity = 3600
    subnet_ids = var.lustre_subnet_us-east-1b
    import_path = "s3://nasanex/Landsat"
}

resource "aws_spot_fleet_request" "hpc-fleet" {
    iam_fleet_role = var.iam_fleet_role
    spot_price = "0.0310"
    allocation_strategy = "lowestPrice"
    target_capacity = 3
    terminate_instances_with_expiration = true
    
    launch_specification {
        instance_type = "c4.large"
        ami = var.amis[var.region]
        key_name = var.key_pair[var.region]
        subnet_id = var.subnets_us-east-1.us-east-1b

        user_data = templatefile("${path.module}/lustre.tmpl", {
                    lustre_file = "${aws_fsx_lustre_file_system.hpc_storage.dns_name}"
        })

        tags = {
            Name = "HPC C4"
        }
    }

    launch_specification {
        instance_type = "m4.large"
        ami = var.amis[var.region]
        key_name = var.key_pair[var.region]
        subnet_id = var.subnets_us-east-1.us-east-1b
       
        tags = {
            Name = "HPC M4"
        }
    }
    depends_on = [aws_fsx_lustre_file_system.hpc_storage]
}

output "lustre_dns_name" {
    value =  aws_fsx_lustre_file_system.hpc_storage.dns_name
}