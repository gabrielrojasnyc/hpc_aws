FROM alpine:3.10.2


ENV terraform_version="0.12.7" \
    aws_version="1.16.229"

# Installs python3 and pip for AWS CLI and bash
RUN apk add python3 py3-pip bash curl

# Download verion 0.12.7 terraform and install it on image && cleanup
RUN wget https://releases.hashicorp.com/terraform/${terraform_version}/terraform_${terraform_version}_linux_amd64.zip && \
unzip terraform_${terraform_version}_linux_amd64.zip -d /usr/bin && \
rm terraform_${terraform_version}_linux_amd64.zip -f && rm -rf /var/cache/apk/*

# Installs awscli 
RUN pip3 install awscli==${aws_version}

ENTRYPOINT [ "/bin/bash", "-l", "-c"]